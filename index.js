// console.log("s24 activity")
let x = 2
let y = 3
const getCube = Math.pow(x, y);

console.log(`The cube of ${x} is ${getCube}`)
// address array
const address = ['Daisy Street', 'Brgy 2','La Carlota City', 'Negros Occidental']

const [street, brgy, city, province] = address

console.log(`I live at ${street}, ${brgy}, ${city}, ${province}.`)

//animal object
const animalInfo = {
	name: "Jorge",
	species: "native pig",
	weight: "1000 kgs",
	measurement: "4ft 6br"
}

let{name, species , weight, measurement} = animalInfo
console.log(`${name} was a ${species}. He weighed at ${weight} with measurement of ${measurement}.`)
//numbers array
let arrayNum = [2, 4, 6, 8, 10];
let currentNum;

arrayNum.forEach((num) => {
	currentNum = num;
	console.log(currentNum);
});

let reduceNum = arrayNum.reduce((x, y) => x + y);
console.log(reduceNum);

// doggo
class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}

let dog = new Dog("Kronos", 3,"Aslol")
console.log(dog)




